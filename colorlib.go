// Created by arsikurin
package colorlib

import (
	//"errors"
	//"fmt"
)

type Styled struct {
	Reset, Bold, Dim, Italic, Underlined, Blink, Inverted, Hidden, Strikethrough, Underlined_double, Underlined_lower string
}

type Fore struct {
	Reset, Black, Red, Green, Yellow, Blue, Magenta, Cyan, Beige, White, Grey, Light_red, Light_green, Light_yellow, Light_blue, Light_magenta, Light_cyan string
}

type Back struct {
	Reset, Black, Red, Green, Yellow, Blue, Magenta, Cyan, Beige, White, Grey, Light_red, Light_green, Light_yellow, Light_blue, Light_magenta, Light_cyan string
}

type ControlCharacters struct {
	Backspace, Tab, Line_feed, Carriage_return string
	// For use inside strings
	// Backslash \\
	// Double quote \"
	// Quote \'
}

var (
	Style        = Styled{"\033[0m", "\033[1m", "\033[2m", "\033[3m", "\033[4m", "\033[5m", "\033[7m", "\033[8m", "\033[9m", "\033[21m", "\033[52m"}
	Fg           = Fore{"\033[0m", "\033[30m", "\033[31m", "\033[32m", "\033[33m", "\033[34m", "\033[35m", "\033[36m", "\033[37m", "\033[38m", "\033[90m", "\033[91m", "\033[92m", "\033[93m", "\033[94m", "\033[95m", "\033[96m"}
	Bg           = Back{"\033[0m", "\033[40m", "\033[41m", "\033[42m", "\033[43m", "\033[44m", "\033[45m", "\033[46m", "\033[47m", "\033[48m", "\033[100m", "\033[101m", "\033[102m", "\033[103m", "\033[104m", "\033[105m", "\033[106m"}
	ControlChars = ControlCharacters{"\b", "\t", "\n", "\r"}
)
